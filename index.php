<?php
    require 'app/autoload.php';
    $_SERVER['PHP_SELF'] = str_replace('index.php', '', $_SERVER['PHP_SELF']);
    $atributo = null;

    if($_GET){
        $url = explode('/', $_GET['url']);
        $controller_acessado = $url[0] . 'Controller';

        if(array_key_exists(1, $url)){
            $metodo_acessado = $url[1];
        }else{
            $metodo_acessado = 'listar';
        }

        if(array_key_exists(2, $url)){
            $atributo = $url[2];
        }
    }else{
        $controller_acessado = 'HomeController';
        $metodo_acessado = 'home';
    }

    $controller = new $controller_acessado();
    $controller->$metodo_acessado($atributo);

    