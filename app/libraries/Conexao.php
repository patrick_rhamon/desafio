<?php

    class Conexao {
        private $host = "localhost";
        private $banco = "projeto_webjump";
        private $porta = "3306";
        private $usuario = "root";
        private $senha = "";
        private $dbh;
        private $stmt;

        public function __construct()
        {
            $dsn = 'mysql:host='.$this->host.';port='.$this->porta.';dbname='.$this->banco;
            $opcoes = [
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ];

            try {
                $this->dbh = new PDO($dsn, $this->usuario, $this->senha, $opcoes);
            } catch(PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
        }

        public function query($sql) {
            $this->stmt = $this->dbh->prepare($sql);
        }

        public function bind($parametro, $valor, $tipo = null) {
            if(is_null($tipo)){
                switch(true){
                    case is_int($valor):
                        $tipo = PDO::PARAM_INT;
                        break;
                    case is_bool($valor):
                        $tipo = PDO::PARAM_BOOL;
                        break;
                    case is_null($valor):
                        $tipo = PDO::PARAM_NULL;
                        break;
                    default:
                        $tipo = PDO::PARAM_STR;
                }
            }

            $this->stmt->bindValue($parametro, $valor, $tipo);
        }

        public function executar() {
            return $this->stmt->execute();
        }

        public function resultado() {
            $this->executar();
            return $this->stmt->fetch(PDO::FETCH_OBJ);
        }

        public function resultados() {
            $this->executar();
            return $this->stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function contarLinhas() {
            return $this->dbh->rowCount();
        }

        public function ultimoCodigoInserido() {
            return $this->dbh->lastInsertId();
        }
    }