<?php 
    const LOG_DESATIVADO = 0;
    const LOG_ACESSO = 1;
    const LOG_ACAO = 2;
    const LOG_GERAL = 3;

    /*** Descrição dos acessos {
        O log de acesso será registrado apenas para acesso das páginas toda vez que for requisitada
        O log de ação irá considerar apenas as ações de insert, update, remove
        O log geral salvará tudo
    } ***/

    class Log {
        private $date;
        private $file_name;
        private $path;
        private $nivel_log;

        public function __construct()
        {
            $this->nivel_log = LOG_DESATIVADO;
            $this->date = date('Ymd');
            $this->file_name = 'log_'.$this->date.'.log';
            $this->path = 'app/log/';
        }

        public function registrar($acao, $mensagem, $destino=null){
            $origem_acesso = $_SERVER['REMOTE_ADDR'];
            $data_acao = date('d/m/Y H:i:s');
            $file = fopen($this->path.$this->file_name, 'a');
            if($destino){
                $write = "ação: $acao\nOrigem: $origem_acesso\nData: $data_acao\nDestino: $destino\nRetorno: $mensagem\n\n\n";
            }else{
                $write = "ação: $acao\nOrigem: $origem_acesso\nData: $data_acao\nRetorno: $mensagem\n\n\n";
            }
            fwrite($file, $write);
            fclose($file);
        }

        public function nivelLog(){
            return $this->nivel_log;
        }
    }