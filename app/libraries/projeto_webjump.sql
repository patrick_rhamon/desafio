-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Ago-2020 às 05:15
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projeto_webjump`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`codigo`, `nome`) VALUES
(1, 'CAMA'),
(2, 'MESA'),
(3, 'BANHO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `codigo` int(11) NOT NULL,
  `SKU` varchar(191) DEFAULT NULL,
  `nome` varchar(191) DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `descricao` longtext DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `url_imagem` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`codigo`, `SKU`, `nome`, `preco`, `descricao`, `quantidade`, `url_imagem`) VALUES
(1, 'teste', 'teste', 10, 'teste', 4, 'public/images/go-logo.png'),
(2, 'novo', 'novo', 25, 'aaaaaaaaaaa', 56, 'public/images/go-logo.png'),
(3, 'sku003', 'TÃªnis do Seninha', 59.58, 'teste', 0, 'public/images/product/15972749015f347b15d227c.jpg'),
(4, 'sku004', 'Secador de Cabelo', 289.9, 'teste', 1, 'public/images/product/15972749915f347b6f2aaed.jpg'),
(5, 'sku005', 'Prato de Porcelana', 99.5, 'Prato de Porcelana', 10, 'public/images/product/15972748105f347aba953e7.jpg'),
(6, 'SKU010', 'FIAT 147 TUNNING', 17850, 'FIAT 147 TUNNING todo revisado pronto para corrida.', 1, 'public/images/product/15972780235f3487473ba50.jpg'),
(7, 'teste', 'teste ingles', 123, 'tes', 321, 'public/images/product/15972883415f34af950d197.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `codigo_produto` int(11) DEFAULT NULL,
  `codigo_categoria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`codigo_produto`, `codigo_categoria`) VALUES
(2, 1),
(2, 3),
(5, 2),
(5, 3),
(3, 1),
(1, 1),
(1, 2),
(4, 3),
(6, 3),
(7, 2);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices para tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD KEY `fk_produtos` (`codigo_produto`),
  ADD KEY `fk_categorias` (`codigo_categoria`);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD CONSTRAINT `fk_categorias` FOREIGN KEY (`codigo_categoria`) REFERENCES `categorias` (`codigo`),
  ADD CONSTRAINT `fk_produtos` FOREIGN KEY (`codigo_produto`) REFERENCES `produtos` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
