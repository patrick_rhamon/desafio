<?php

    class Produto {
        private $db;
        public $codigo;
        public $sku;
        public $nome;
        public $preco;
        public $descricao;
        public $quantidade;
        public $url_imagem;
        public $categorias;

        public function __construct(){
            $this->db = new Conexao();
            $this->categorias = [];
        }

        public function listar(){
            $this->db->query("SELECT * FROM produtos");
            $produtos = $this->db->resultados();
            foreach($produtos as $produto){
                $produto->categorias = $this->buscarCategorias($produto->codigo);
            }
            return $produtos;
        }

        public function salvar($dados){
            $this->db->query("INSERT INTO produtos (codigo, nome, sku, preco, quantidade, descricao, url_imagem) VALUES (:codigo, :nome, :sku, :preco, :quantidade, :descricao, :url_imagem)");
            $this->db->bind(":codigo", $dados['codigo']);
            $this->db->bind(":nome", $dados['nome']);
            $this->db->bind(":sku", $dados['sku']);
            $this->db->bind(":preco", $dados['preco']);
            $this->db->bind(":quantidade", $dados['quantidade']);
            $this->db->bind(":descricao", $dados['descricao']);
            $this->db->bind(":url_imagem", $dados['url_imagem']);

            if($this->db->executar()){
                return true;
            }
            return false;
        }

        public function atualizar($dados){
            $this->db->query("UPDATE produtos SET nome=:nome, sku=:sku, preco=:preco, quantidade=:quantidade, descricao=:descricao, url_imagem=:url_imagem where codigo=:codigo");
            $this->db->bind(":codigo", $dados['codigo']);
            $this->db->bind(":nome", $dados['nome']);
            $this->db->bind(":sku", $dados['sku']);
            $this->db->bind(":preco", $dados['preco']);
            $this->db->bind(":quantidade", $dados['quantidade']);
            $this->db->bind(":descricao", $dados['descricao']);
            $this->db->bind(":url_imagem", $dados['url_imagem']);
            
            if($this->db->executar()){
                return true;
            }
            return false;
        }

        public function buscar($codigo) {
            $this->db->query("SELECT * FROM produtos where codigo = :codigo");
            $this->db->bind(":codigo", $codigo);
            $produto = $this->db->resultado();
            if($produto)
                $produto->categorias = $this->buscarCategorias($produto->codigo);
            return $produto;
        }

        public function remover($codigo) {
            $this->removerCategorias($codigo);
            $this->db->query("DELETE FROM produtos where codigo = :codigo");
            $this->db->bind(":codigo", $codigo);
            return $this->db->executar();
        }

        public function removerCategorias($produto_id){
            $this->db->query("DELETE FROM produtos_categorias where codigo_produto = :codigo_produto");
            $this->db->bind(":codigo_produto", $produto_id);
            return $this->db->executar();
        }

        public function vincularCategorias($produto_id, $categoria_id){
            $this->db->query("INSERT INTO produtos_categorias (codigo_produto, codigo_categoria) VALUES (:codigo_produto, :codigo_categoria)");
            $this->db->bind(":codigo_produto", $produto_id);
            $this->db->bind(":codigo_categoria", $categoria_id);

            if($this->db->executar()){
                return true;
            }
            return false;
        }

        public function ultimoId(){
            $this->db->query("SELECT max(codigo) as ultimo_id FROM produtos");
            return $this->db->resultado();
        }

        public function buscarCategorias($produto_id){
            $this->db->query("SELECT c.* FROM categorias c inner join produtos_categorias pc on pc.codigo_categoria = c.codigo where pc.codigo_produto = :codigo_produto");
            $this->db->bind(":codigo_produto", $produto_id);
            return $this->db->resultados();
        }
    }