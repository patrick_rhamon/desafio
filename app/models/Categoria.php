<?php

    class Categoria {
        private $db;
        public $nome;
        public $codigo;

        public function __construct(){
            $this->db = new Conexao();
        }

        public function listar(){
            $this->db->query("SELECT * FROM categorias");
            return $this->db->resultados();
        }

        public function salvar($dados){
            $this->db->query("INSERT INTO categorias (codigo, nome) VALUES (:codigo, :nome)");
            $this->db->bind(":codigo", $dados['codigo']);
            $this->db->bind(":nome", $dados['nome']);
            
            if($this->db->executar()){
                return true;
            }
            return false;
        }

        public function atualizar($dados){
            $this->db->query("UPDATE categorias SET codigo = :codigo, nome = :nome where codigo = :id");
            $this->db->bind(":codigo", $dados['codigo']);
            $this->db->bind(":nome", $dados['nome']);
            $this->db->bind(":id", $dados['id']);
            
            if($this->db->executar()){
                return true;
            }
            return false;
        }

        public function buscar($codigo) {
            $this->db->query("SELECT * FROM categorias where codigo = :codigo");
            $this->db->bind(":codigo", $codigo);
            return $this->db->resultado();
        }

        public function remover($codigo) {
            $this->db->query("DELETE FROM categorias where codigo = :codigo");
            $this->db->bind(":codigo", $codigo);
            return $this->db->executar();
        }
    }