
<!doctype html>
<html ⚡>
    <head>
        <?php include 'app/views/includes/head.php' ?>
    </head>
    <body>
        <!-- Header --> 
            <?php include 'app/views/includes/menu.php' ?>
        <!-- Header -->
        <!-- Main Content -->
            <main class="content">
                <h1 class="title new-item"><?= $titulo_pagina ?></h1>

                <?php if($produto): ?>
                    <form action="../alterar" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?= $produto->codigo ?>">
                <?php else: ?>
                    <form action="incluir" method="post" enctype="multipart/form-data">
                <?php endif; ?>
                    <div class="input-field">
                        <label for="sku" class="label">SKU do Produto</label>
                        <input type="text" id="sku" name="sku" class="input-text" value="<?= $produto->SKU ?? '' ?>" /> 
                    </div>
                    <div class="input-field">
                        <label for="nome" class="label">Nome do Produto</label>
                        <input type="text" id="nome" name="nome" class="input-text" value="<?= $produto->nome ?? '' ?>" /> 
                    </div>
                    <div class="input-field">
                        <label for="preco" class="label">Preço do Produto</label>
                        <input type="text" id="preco" name="preco" class="input-text" value="<?= $produto ? number_format($produto->preco, 2, ',', '.') : '' ?>" /> 
                    </div>
                    <div class="input-field">
                        <label for="quantidade" class="label">Quantidade do Produto</label>
                        <input type="text" id="quantidade" name="quantidade" class="input-text" value="<?= $produto->quantidade ?? '' ?>" /> 
                    </div>
                    <div class="input-field">
                        <label for="categoria" class="label">Categorias</label>
                        <select multiple id="categoria" name="categoria[]" class="input-text">
                        <?php foreach($categorias as $categoria): ?>
                            <option value="<?= $categoria->codigo ?>"
                                <?php if($produto): ?>
                                    <?php foreach($produto->categorias as $produto_categoria): ?>
                                        <?php if($categoria->codigo == $produto_categoria->codigo): ?>
                                            selected
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            ><?= $categoria->nome ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                    <div class="input-field">
                        <label for="descricao" class="label">Descrição do Produto</label>
                        <textarea id="descricao" name="descricao" class="input-text"> <?= $produto->descricao ?? '' ?></textarea>
                    </div>
                    <div class="input-field">
                        <label for="url_imagem" class="label">Quantidade do Produto</label>
                        <input type="hidden" id="url_imagem" name="url_imagem" value="<?= $produto->url_imagem ?? '' ?>" /> 
                        <input type="file" id="arquivo" name="arquivo" class="" />
                        <Br/><small>*Dimensões da imagem recomendada: width/largura: 164 | height/altura: 145<br>**Formatos de imagens aceitos: .jpg, .jpeg, .gif, .png</small>
                    </div>
                    <div class="actions-form">
                        <?php if($produto): ?>
                            <a href="../../produto" class="action back">Back</a>
                        <?php else: ?>
                            <a href="../produto" class="action back">Back</a>
                        <?php endif; ?>
                        <input class="btn-submit btn-action" type="submit" value="Save Product" />
                    </div>
                </form>
            </main>
        <!-- Main Content -->

        <!-- Footer -->
            <footer>
                <?php include 'app/views/includes/footer.php'; ?>
            </footer>
        <!-- Footer -->
    </body>
</html>
