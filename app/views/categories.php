
<!doctype html>
<html ⚡>
    <head>
        <?php include 'app/views/includes/head.php' ?>
    </head>
    <body>
        <!-- Header --> 
            <?php include 'app/views/includes/menu.php' ?>
        <!-- Header -->
        <!-- Main Content -->
            <main class="content">
                <div class="header-list-page">
                    <h1 class="title"><?= $titulo_pagina ?></h1>
                    <a href="categoria/novo" class="btn-action">Nova Categoria</a>
                </div>
                <table class="data-grid">
                    <tr class="data-row">
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Name</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Code</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Actions</span>
                        </th>
                    </tr>
                    <?php foreach($categorias as $categoria): ?>
                        <tr class="data-row">
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content"><?= $categoria->nome; ?></span>
                            </td>
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content"><?= $categoria->codigo; ?></span>
                            </td>
                            <td class="data-grid-td">
                                <div class="actions">
                                    <a href="categoria/novo/<?= $categoria->codigo; ?>"> Editar </a> | 
                                    <a href="categoria/remover/<?= $categoria->codigo; ?>"> Remover </a>
                                </div>
                            </td>
                        </tr>
                    <?php  endforeach; ?>
                </table>
            </main>
        <!-- Main Content -->

        <!-- Footer -->
            <footer>
                <?php include 'app/views/includes/footer.php'; ?>
            </footer>
        <!-- Footer -->
    </body>
</html>




