<!doctype html>
<html ⚡>
    <head>
        <?php include 'app/views/includes/head.php' ?>
    </head>
    <body>
        <!-- Header --> 
            <?php include 'app/views/includes/menu.php' ?>
        <!-- Header -->
        <!-- Main Content -->
            <main class="content">
                <div class="header-list-page">
                    <h1 class="title"><?= $titulo_pagina ?></h1>
                    <a href="produto/novo" class="btn-action">Novo Produto</a>
                </div>
                <table class="data-grid">
                    <tr class="data-row">
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Name</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">SKU</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Price</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Quantity</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Categories</span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content">Actions</span>
                        </th>
                    </tr>
                    
                    <?php foreach($produtos as $produto): ?>
                        <tr class="data-row">
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content"><?= $produto->nome ?></span>
                            </td>
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content"><?= $produto->SKU ?></span>
                            </td>
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content">R$ <?= number_format($produto->preco, 2, ',', '.') ?></span>
                            </td>
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content"><?= $produto->quantidade ?></span>
                            </td>
                            <td class="data-grid-td">
                                <span class="data-grid-cell-content">
                                    <?php foreach($produto->categorias as $categoria): ?>
                                        <?= $categoria->nome ?> <Br />
                                    <?php endforeach; ?>
                                </span>
                            </td>
                            <td class="data-grid-td">
                                <div class="actions">
                                    <a href="produto/novo/<?= $produto->codigo; ?>"> Editar </a> | 
                                    <a href="produto/remover/<?= $produto->codigo; ?>"> Remover </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </main>
        <!-- Main Content -->

        <!-- Footer -->
            <footer>
                <?php include 'app/views/includes/footer.php'; ?>
            </footer>
        <!-- Footer -->
    </body>
</html>