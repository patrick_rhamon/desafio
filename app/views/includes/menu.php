<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
    <div class="close-menu">
        <a on="tap:sidebar.toggle">
            <img src="<?=$_SERVER['PHP_SELF']?>/public/images/bt-close.png" alt="Close Menu" width="24" height="24" />
        </a>
    </div>
    <a href="<?= $_SERVER['PHP_SELF'] ?>"><img src="<?= $_SERVER['PHP_SELF'] ?>public/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
    <div>
        <ul>
            <li><a href="categoria" class="link-menu">Categorias</a></li>
            <li><a href="produto" class="link-menu">Produtos</a></li>
        </ul>
    </div>
</amp-sidebar>
<header>
    <div class="go-menu">
        <a on="tap:sidebar.toggle">☰</a>
        <a href="<?= $_SERVER['PHP_SELF'] ?>" class="link-logo"><img src="<?=$_SERVER['PHP_SELF']?>public/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
    </div>
    <div class="right-box">
        <span class="go-title">Painel Administrativo</span>
    </div>    
</header>  