
<!doctype html>
<html ⚡>
    <head>
        <?php include 'app/views/includes/head.php' ?>
    </head>
    <body>
        <!-- Header --> 
            <?php include 'app/views/includes/menu.php' ?>
        <!-- Header -->
        <!-- Main Content -->
            <main class="content">
                <h1 class="title new-item"><?= $titulo_pagina ?></h1>
                <?php if($categoria): ?>
                    <form action="../alterar" method="post">
                        <input type="hidden" name="id" value="<?= $categoria->codigo ?>">
                <?php else: ?>
                    <form action="incluir" method="post">
                <?php endif; ?>
                    <div class="input-field">
                        <label for="category-code" class="label">Codigo da Categoria</label>
                        <input type="text" id="codigo" name="codigo" class="input-text" value="<?= $categoria->codigo ?? '' ?>" />
                    </div>
                    <div class="input-field">
                        <label for="category-name" class="label">Nome da Categoria</label>
                        <input type="text" id="nome" name="nome" class="input-text" value="<?= $categoria->nome ?? '' ?>" />
                    </div>
                    <div class="actions-form">
                        <?php if($categoria): ?>
                            <a href="../../categoria" class="action back">Back</a>
                        <?php else: ?>
                            <a href="../categoria" class="action back">Back</a>
                        <?php endif; ?>
                        <input class="btn-submit btn-action"  type="submit" value="Save" />
                    </div>
                </form>
            </main>
        <!-- Main Content -->

        <!-- Footer -->
            <footer>
                <?php include 'app/views/includes/footer.php'; ?>
            </footer>
        <!-- Footer -->
    </body>
</html>