
<!doctype html>
<html ⚡>
    <head>
        <?php include 'app/views/includes/head.php' ?>
    </head>
    <body>
        <!-- Header --> 
            <?php include 'app/views/includes/menu.php' ?>
        <!-- Header -->
        <!-- Main Content -->
            <main class="content">
                <div class="header-list-page">
                    <h1 class="title"><?= $titulo_pagina; ?></h1>
                </div>
                <div class="infor">
                    Você tem <?= $total_produtos ?> produtos cadastrados na sua loja: <a href="produto/novo" class="btn-action">Novo Produto</a>
                </div>
                <ul class="product-list">
                    <?php foreach($produtos as $produto): ?>
                        <li>
                            <div class="product-image">
                                <img src="<?= $produto->url_imagem ?>" layout="responsive" width="164" height="145" alt="<?= $produto->nome ?>" />
                            </div>
                            <div class="product-info">
                                <div class="product-name"><span><?= $produto->nome ?></span></div>
                                <div class="product-price"><span class="special-price"><?= $produto->quantidade > 1 ? "$produto->quantidade disponíveis" : ($produto->quantidade > 0 ? 'Última Unidade' : 'Indisponível') ?></span> <span>R$ <?= number_format($produto->preco, 2, ',', '.') ?></span></div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </main>
        <!-- Main Content -->

        <!-- Footer -->
            <footer>
                <?php include 'app/views/includes/footer.php'; ?>
            </footer>
        <!-- Footer -->
    </body>
</html>
