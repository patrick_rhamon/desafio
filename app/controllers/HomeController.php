<?php

    class HomeController {
        public function __construct()
        {
            $this->produtoModel = new Produto();
            $this->log = new Log();
        }
        
        function home($atributo){
            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', 'Acesso a tela do dashboard', 'app/views/dashboard.php');
            }
            $titulo_pagina = 'Dashboard';
            $produtos = $this->produtoModel->listar();
            $total_produtos = count($produtos);
            include 'app/views/dashboard.php';
        }

        function dashboard($atributo){
            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', 'Acesso a tela do dashboard', 'app/views/dashboard.php');
            }
            $titulo_pagina = 'Dashboard';
            $produtos = $this->produtoModel->listar();
            $total_produtos = count($produtos);
            include 'app/views/dashboard.php';
        }
    }