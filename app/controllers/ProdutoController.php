<?php
    class ProdutoController {
        public function __construct()
        {
            $this->produtoModel = new Produto();
            $this->categoriaModel = new Categoria();
            $this->log = new Log();
        }

        public function listar() {
            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', 'Acesso a tela de listar produto', 'app/views/products.php');
            }

            $titulo_pagina = 'Listar Produto';
            $produtos = $this->produtoModel->listar();
            include 'app/views/products.php';
        }

        public function novo($atributo) {
            $categorias = $this->categoriaModel->listar();
            $produto = $this->produtoModel->buscar($atributo);
            $titulo_pagina = 'Novo Produto';
            $mensagem = 'Acesso a tela de novo produto';
            if($produto){
                $titulo_pagina = 'Alterar Produto';
                $mensagem = 'Acesso a tela de alterar produto';
            }

            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', $mensagem, 'app/views/addProduct.php');
            }
            include 'app/views/addProduct.php';
        }

        public function incluir(){
            $form = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if(isset($form)){
                if($this->log->nivelLog() == LOG_ACAO || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('inserir', 'Inseção do produto: '.$form['nome'].' | SKU(codigo): '.$form['sku']);
                }
                $ultimo_produto_inserido = $this->produtoModel->ultimoId();
                $ultimo_id_inserido = $ultimo_produto_inserido->ultimo_id;
                $ultimo_id_inserido++;
                $categorias = [];
                if(key_exists('categoria', $form))
                    $categorias = $form['categoria'];

                if(isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0){
                    $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                    $nome_imagem = $_FILES[ 'arquivo' ][ 'name' ];
                    $extensao = pathinfo($nome_imagem, PATHINFO_EXTENSION);
                    $extensao = strtolower($extensao);

                    if(strstr('.jpg;.jpeg;.gif;.png', $extensao)){
                        $novo_nome_imagem = uniqid(time()).'.'.$extensao;
                        $destino = 'public/images/product/'.$novo_nome_imagem;

                        if(@move_uploaded_file($arquivo_tmp, $destino)){
                            $url_imagem = $destino;
                        }else{
                            $url_imagem = null;
                        }
                    }
                }else{
                    $url_imagem = 'public/images/go-logo.png';
                }

                $dados = [
                    'codigo' => intval($ultimo_id_inserido),
                    'nome' => trim($form['nome']),
                    'sku' => trim($form['sku']),
                    'preco' => trim(number_format(str_replace(',', '.', $form['preco']), 2, '.', '')),
                    'quantidade' => trim($form['quantidade']),
                    'descricao' => trim($form['descricao']),
                    'url_imagem' => $url_imagem,
                ];

                if($this->produtoModel->salvar($dados)){
                    $ultimo_produto = $this->produtoModel->ultimoId();
                    $ultimo_id = $ultimo_produto->ultimo_id;
                    $produto = $this->produtoModel->buscar($ultimo_id);
                    $this->produtoModel->removerCategorias($produto->codigo);
                    foreach($categorias as $item_id){
                        $categoria = $this->categoriaModel->buscar($item_id);
                        $vinculo = $this->produtoModel->vincularCategorias($produto->codigo, $categoria->codigo);

                        if(!$vinculo)
                            die('Erro ao salvar ao vincular produto/categoria.');
                    }
                    header("Location: ../produto");
                }else{
                    die('Erro ao salvar o produto.');
                }
            }
        }

        public function alterar(){
            $form = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if(isset($form)){
                if($this->log->nivelLog() == LOG_ACAO || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('alterar', 'Alteração do produto: '.$form['nome'].' | SKU(codigo): '.$form['sku']);
                }
                $categorias = [];
                if(key_exists('categoria', $form))
                    $categorias = $form['categoria'];

                if(isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0){
                    $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                    $nome_imagem = $_FILES[ 'arquivo' ][ 'name' ];
                    $extensao = pathinfo($nome_imagem, PATHINFO_EXTENSION);
                    $extensao = strtolower($extensao);

                    if(strstr('.jpg;.jpeg;.gif;.png', $extensao)){
                        $novo_nome_imagem = uniqid(time()).'.'.$extensao;
                        $destino = 'public/images/product/'.$novo_nome_imagem;

                        if(@move_uploaded_file($arquivo_tmp, $destino)){
                            $url_imagem = $destino;
                        }else{
                            $url_imagem = null;
                        }
                    }

                    if(file_exists($form['url_imagem']) && $form['url_imagem'] != 'public/images/go-logo.png') {
                        unlink($form['url_imagem']);
                    }
                }else if($form['url_imagem']){
                    $url_imagem = $form['url_imagem'];
                }else{
                    $url_imagem = 'public/images/go-logo.png';
                }

                $dados = [
                    'codigo' => $form['id'],
                    'nome' => trim($form['nome']),
                    'sku' => trim($form['sku']),
                    'preco' => trim(number_format(str_replace(',', '.', $form['preco']), 2, '.', '')),
                    'quantidade' => trim($form['quantidade']),
                    'descricao' => trim($form['descricao']),
                    'url_imagem' => $url_imagem,
                ];
                                
                if($this->produtoModel->atualizar($dados)){
                    $produto = $this->produtoModel->buscar($form['id']);
                    $this->produtoModel->removerCategorias($produto->codigo);
                    foreach($categorias as $item_id){
                        $categoria = $this->categoriaModel->buscar($item_id);
                        $vinculo = $this->produtoModel->vincularCategorias($produto->codigo, $categoria->codigo);

                        if(!$vinculo)
                            die('Erro ao salvar ao vincular produto/categoria.');
                    }
                    header("Location: ../produto");
                }else{
                    die('Erro ao alterar o produto.');
                }
            }
        }

        public function remover($atributo){
            $produto = $this->produtoModel->buscar($atributo);
            if($produto){
                if($this->log->nivelLog() == LOG_ACAO || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('remover', 'Remoção do produto: '.$produto->nome.' | SKU(codigo): '.$produto->SKU);
                }
                $this->produtoModel->remover($produto->codigo);
                header("Location: ../../produto");
            }else{
                die('Produto não encontrado.');
            }
        }
    }