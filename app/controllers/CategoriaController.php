<?php

    class CategoriaController {

        public function __construct()
        {
            $this->categoriaModel = new Categoria();
            $this->log = new Log();
        }

        public function listar(){
            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', 'Acesso a tela de listar categoria', 'app/views/categories.php');
            }
            $titulo_pagina = 'Listar Categoria';
            $categorias = $this->categoriaModel->listar();
            include 'app/views/categories.php';
        }

        public function novo($atributo){
            $categoria = $this->categoriaModel->buscar($atributo);
            $titulo_pagina = 'Nova Categoria';
            $mensagem = 'Acesso a tela de nova categoria';
            if($categoria){
                $titulo_pagina = 'Alterar Categoria';
                $mensagem = 'Acesso a tela de alterar categoria';
            }

            if($this->log->nivelLog() == LOG_ACESSO || $this->log->nivelLog() == LOG_GERAL){
                $this->log->registrar('acessar', $mensagem, 'app/views/addCategory.php');
            }
            include 'app/views/addCategory.php';
        }
        
        public function incluir(){
            $form = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if(isset($form)){
                if($this->log->nivelLog() == 2 || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('inserir', 'Inserção da categoria: '.$form['nome'].' | codigo: '.$form['codigo']);
                }
                $dados = [
                    'nome' => trim($form['nome']),
                    'codigo' => trim($form['codigo']),
                ];
                
                if($this->categoriaModel->salvar($dados)){
                    header("Location: ../categoria");
                }else{
                    die('Erro ao salvar a categoria.');
                }
            }
        }

        public function alterar(){
            $form = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if(isset($form)){
                if($this->log->nivelLog() == 2 || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('alterar', 'Alteração da categoria: '.$form['nome'].' | codigo: '.$form['codigo']);
                }
                $dados = [
                    'id' => trim($form['id']),
                    'nome' => trim($form['nome']),
                    'codigo' => trim($form['codigo']),
                ];

                if((intval($form['id']) != intval($form['codigo'])) && ($this->categoriaModel->buscar($form['codigo']))){
                    die('Já existe categoria com o código informado.');
                }
                
                if($this->categoriaModel->atualizar($dados)){
                    header("Location: ../categoria");
                }else{
                    die('Erro ao alterar a categoria.');
                }
            }
        }

        public function remover($atributo){
            $categoria = $this->categoriaModel->buscar($atributo);
            if($categoria){
                if($this->log->nivelLog() == 2 || $this->log->nivelLog() == LOG_GERAL){
                    $this->log->registrar('remover', 'Remoção da categoria: '.$categoria->nome.' | codigo: '.$categoria->codigo);
                }
                $this->categoriaModel->remover($categoria->codigo);
                header("Location: ../../categoria");
            }else{
                die('Categoria não encontrada.');
            }
        }
    }