<?php

    class PaginasController {
        protected $listar_produto;
        protected $novo_produto;
        protected $listar_categoria;
        protected $nova_categoria;
        protected $dashboard;

        public function __construct()
        {
            $this->dashboard = '?controller=paginas&action=dashboard';
            $this->listar_produto = '?controller=paginas&action=listarProduto';
            $this->novo_produto = '?controller=paginas&action=novoProduto';
            $this->listar_categoria = '?controller=paginas&action=listarCategoria';
            $this->nova_categoria = '?controller=paginas&action=novaCategoria';
        }

        function home(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Dashboard';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/dashboard.php';
        }

        function dashboard(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Dashboard';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/dashboard.php';
        }

        function listarProduto(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Listar Produto';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/products.php';
        }
        
        function novoProduto(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Novo Produto';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/addProduct.php';
        }

        function listarCategoria(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Listar Categoria';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/categories.php';
        }
        
        function novaCategoria(){
            $dashboard = $this->dashboard;
            $titulo_pagina = 'Nova Categoria';
            $listar_categoria = $this->listar_categoria;
            $listar_produto = $this->listar_produto;
            $nova_categoria = $this->nova_categoria;
            $novo_produto = $this->novo_produto;
            include 'app/views/addCategory.php';
        }

    }