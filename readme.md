# Você quer ser um desenvolvedor Backend na Web Jump?
Criamos esse teste para avaliar seus conhecimentos e habilidades como desenvolvedor backend.

# O teste
O desafio é desenvolver um sistema de gerenciamento de produtos. Esse sistema será composto de um cadastro de produtos e categorias. Os requisitos desse sistema estão listados nos tópicos abaixo.
Não existe certo ou errado, queremos saber como você se sai em situações reais como esse desafio.

# Instruções
- O foco principal do nosso teste é o backend. Para facilitar você poderá utilizar os arquivos html  disponíveis no diretório assets
- Crie essa aplicação como se fosse uma aplicação real, que seria utilizada pelo WebJump
- Fique à vontade para usar bibliotecas/componentes externos
- Não utilize nenhum Framework, tais como Laravel, Symphony
- Seguir princípios **SOLID** 
- Utilize boas práticas de programação
- Utilize boas práticas de git
- Documentar como rodar o projeto
- Crie uma documentação simples comentando sobre as tecnologias, versões e soluções adotadas

# Requisitos
- O sistema deverá ser desenvolvido utilizando a linguagem PHP (de preferência a versão mais nova) ou outra linguagem se assim foi especificado para sua avaliação por nossa equipe.
- Você deve criar um CRUD que permita cadastrar as seguintes informações:
	- **Produto**: Nome, SKU (Código), preço, descrição, quantidade e categoria (cada produto pode conter uma ou mais categorias)
	- **Categoria**: Código e nome.
- Salvar as informações necessárias em um banco de dados (relacional ou não), de sua escolha

# Opcionais
- Gerar logs das ações
- Testes automatizados com informação da cobertura de testes
- Upload de imagem no cadastro de produtos

# O que será avaliado
- Estrutura e organização do código e dos arquivos
- Soluções adotadas
- Tecnologias utilizadas
- Qualidade
- Padrões PSR, Design Patterns
- Enfim, tudo será observado e levado em conta

# Como iniciar o desenvolvimento
- Fork esse repositório na sua conta do BitBucket.
- Crie uma branch com o nome desafio

# Como enviar seu teste
Envie um email para [carreira@webjump.com.br] com o link do seu repositório.

Se o seu repositório for privado, conceda acesso ao email [codereview@webjump.com.br].

Qualquer dúvida sobre o teste, fique a vontade para entrar em contato conosco.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# TECNOLOGIAS UTILIZADAS
    - Versão do PHP: 7.3.8;
    - Banco de Dados: MariaDB;
    - Versão do Banco: 10.4.6;
    - Estruturação do código utilizando MVC simples, sem a utilização de repositório para separar o modelo do banco;
    - Utilização do autoload dos modelos e controllers;
    - Aplicação simples de URL amigável com a opção de passar apenas 1 parâmentro;

# INSTRUÇÕES DE UTILIZAÇÃO
*-* Restauração do Banco da Dados:
    - Dentro da pasta app/libraries tem um arquivo chamado "projeto_webjump.sql", basta restaurar o banco que já virá com a tabela e alguns registros inseridos;
*-* Utilizando a Aplicação:
    - Copiar o projeto dentro da raiz do servidor Web e rodar a aplicação, as interações serão feitas a partir da página index.php, onde a URL está amigável, logo, a navegação de todo o sistema será pelas ações dos botões na tela;
    - Para acessar o dashboard, basta acessar a raiz do projeto pelo navegador ou clicar na logo no topo da página;
    - As navegações dos menus estão encaminhando para listagem das respectivas páginas descritas (categoria e produto);

# INFORMAÇÕES ADICIONAIS
    - As imagens do produto são salvas na pasta /public/images/product/;
    - Os logs de acesso/ações são salvos na pasta /app/log/ com a data do dia e para a sua utilização e configuração, basta acessar o arquivo /app/libraries/Log.php definir no construct o nível do log (desativado, acesso, ação, geral) todos com as constantes descritas no arquivo.
    - O campo SKU da tabela produto é do tipo string, foi criado um campo de codigo adicional que a principio seria auto-incremento, mas foi deixado para a aplicação recuperar o último código inserido e adicionar mais um.

# FERRAMENTAS UTILIZADAS PARA DESENVOLVER
    - Editor de Código: Visual Studio Code;
    - Servido PHP/MySQL: XAMPP (APACHE / MariaDB);
    - Acesso ao Banco: HeideSQL e PHPMyAdmin;
    - Repositório: Bitbucket/GitHub;